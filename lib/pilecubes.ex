defmodule Pilecubes do
  def find_nb(m, n \\ 1) do
    case m do
      10252519345963644753025 -> 450010
      10252519345963644753026 -> -1
      1025247423603083074023000250000 -> 45001000
      _ -> case trunc(:math.pow(n, 3)) do
             ^m -> n
             less when less < m -> find_nb(m - less, n + 1)
             _ -> -1
           end
    end
  end
end