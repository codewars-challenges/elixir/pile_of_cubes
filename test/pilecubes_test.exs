defmodule PilecubesTest do
  use ExUnit.Case
  doctest Pilecubes

  def testFindNb(numtest, m, ans) do
    IO.puts("Test #{numtest}")
    assert  Pilecubes.find_nb(m) == ans
  end

  test "find_nb" do
    testFindNb  1, 4183059834009, 2022
    testFindNb  2, 24723578342962, -1
    testFindNb  3, 135440716410000, 4824
    testFindNb  4, 40539911473216, 3568
    testFindNb  5, 2675962482846436, 10171
    testFindNb  6, 1025292944081385001, 45001
                   2305843009213693951
  end
end
